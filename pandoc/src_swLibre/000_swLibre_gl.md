---
title: Taller Escornabot 2021
subtitle: Software e Hardware Libres
author:
- Bricolabs
tags: [Robótica, Escornabot]
date: octubre-2021
lang: gl-ES
abstract: |
    Algunhas aclaracións sobor do software e hardware libres
---

A asociación Bricolabs ten como un dos seus obxetivos principais a difusión e promoción do uso do Software e Hardware Libres. Neste documento tentamos dar unha introducción a esos conceptos.

# O Software Libre

O *Software Libre* é algo moi concreto: un software é *Software Libre* se, e só se garante **ao usuario final** as ___Catro Liberdades___:

0. Executar o programa como se desexe e con calquer propósito
1. Estudar como traballa o programa e modificalo para que faga o que desexemos
2. Redistribuir copias para axudar a outros
3. Redistribuir copias modificadas para terceiros

As veces, como resumo, fálase das liberdades de **Uso, Modificación e Copia**.

A definición do *Software Libre* céntrase nos dereitos do usuario que non poden ser limitados polo programador ou polo distribuidor, pola contra no software privativo normalmente establecensen limitacións ao que o usuario pode facer con ese software.

As liberdades 1 e 3 implican que hai que dar ao usuario acceso completo ao código fonte.

Podemos falar de diversas licenzas de software e de canto se achegan ou afastan da definición do *Software Libre*, mais se no cumplen a condición de garantir ***As Catro Liberdades*** no son *Software Libre*

Específicamente, o *Open Source Software* (OSS ou Software de Código Aberto en galego), no é *Software Libre*.

## Uso comercial

En inglés *Free Software* pode significar indistintamente *Software Gratis* ou *Software Libre*, isto ven provocando todo tipo de erros e malentendidos. De feito agora fálase mais ben de ***Free Libre Software*** e non de *Free Software* para que non haxa dúbidas de que se fala de *Free as in Freedom*. No galego non temos ese problema, mais lembremos sempre que o *Software Libre* no ten por que ser software gratuito, e que non todo o software gratis é *Software Libre*.

En realidade ***As Catro Liberdades*** garanten o dereito de Uso Comercial. As liberdades 2 e 3 permiten a redistribución de cualquer *Software Libre*, e ninguén nos impide cobrar polo software distribuido.

Mais para que o software que redistribues sexa *Software Libre* non podes limitar de xeito ningún ***As Catro Liberdades***.

## Software Complexo

O normal é que o software esté composto de moitas partes: bibliotecas (ou librerias), sistemas operativos, programas de utilidade etc. etc.

O conxunto do software será libre se, e só se todos os seus compoñentes son *Software Libre*

## Copyleft

O Copyleft é un método para exixir que as versións modificadas e/ou extendidas dun *Software Libre* sexan tamén *Software Libre*; é decir exíxese que quenquer que redistribua o software, con ou sin cambios, garante aos receptores ***As Catro Liberdades***.

Un software, ou mais ben unha licenza, que especifica que as copias modificadas non son libres (limitando de calquer xeito ***As Catro Liberdades***) no é unha Licenza Libre, no é *Software Libre*

Se a licenza exixe que as versións redistribuidas sexan tamén libres, entón si que se poden considerar *Software Libre*.

# Hardware Libre

Parece desexable (siquiera para moita xente, incluidos nos) extender a definición do *Software Libre* ao Hardware. O problema é que o software es unha "propiedade intelectual" casi pura. Tanto o deseño (código fonte) como o producto final (executable) dun software son doados de copiar e distribuir, prácticamente sen custo.

O hardware non é así para nada, sempre hai custos asociados que ademais poden ser ben grandes. Esta característica por si soa xa impediría usar a definición de *Software Libre* para o *Hardware Libre* sen moitas modificacións.

O que si parece transladable é a filosofía do *Software Libre*. Por exemplo, non sería difícil aplicar a filosofía e a definición do *Software Libre* aos deseños do Hardware.

Dada a natureza do hardware e os seus problemas asociados veremos que hai varias líneas de traballo en marcha na definición do *Hardware Libre*. Por outra banda non se avanza igual en todos os campos, ainda que hai proxectos libres de distintas áreas da tecnoloxía, a meirande parte dos  intentos de definición do hardware libre e da implementación de proxectos libres céntranse ou están moi relacionados coa electrónica.

## Dispositivos de lóxica programable

No hardware ademais dos problemas de custes asociados do que falamos antes, adoitamos falar de sistemas complexos (análogos ao software complexo do que tamén falamos) Pensemos en todos os compoñentes necesarios para unha impresora 3D ou un automóvil por citar un proxecto "mais ambicioso"". 

Tomemos un exemplo ben coñecido de electrónica: a placa Arduino. É un exemplo típico do Hardware Libre, mais o microprocesador ATMega que se usa para implementar o Arduino non é libre. Do mismo xeito unha impresora 3D usa motores, servos, etc que non son hardware libre. Se pensamos nun automóvil teremos patentes mesmo para distintos tipos de aceiro.

No que respecta á electrónica, nos últimos anos temos visto grandes avances na definición de dispositivos de lóxica programable **libres**, ahí temos por exemplo a FPGA Alhambra (agora xa hai mais FPGA libres) ou o software de programación de FPGA Icestudio. Cunha FPGA sería posible deseñar e implementar unha placa con as mesmas funcionalidades que un Arduino, ou que unha tarjeta gráfica de ordenador, ou mesmo o que o procesador do propio ordenador, por citar algunhas posibilidades.

Estes avances fan posible el desenrolo de dispositivos electrónicos que seran completamente libres, no sentido de non ter que usar necesariamente componentes pechados para a implementación.

## Open Hardware

Igual que xurde o _Open Source_ coma un intento de adoptar o ciclo de desenrolo do *Software Libre* para o software privativo. Tamén se fala de *Open Hardware* e cada vez hai mais interés (e presión) por parte da industria para definir casos de *Open Hardware*. Hoxe en dia xa temos despliegues de *Software Defined Networks* (SDN) por parte dos operadores de telecomunicacións e de proveedores de servicios na nube. Ahí interesa ter o hardware: servidores, routers, estacións base de telefonía, etc. etc. definidos como _Open Hardware_, de xeito que a rede non dependa dun só fabricante, baixen os custes, haxa repuestos compatibles, etc. etc.

É debatible ata que punto estes movementos da industria encaixan coa filosofía do *Software Libre* de antepoñer a todo os dereitos do usuario final. Pero en calquer caso a tendencia é esa, e tamén pode supoñer unha oportunidade para o *Hardware Libre*.


# Proxecto Escornabot

No proxecto Escornabot tentamos seguir o máximo posible á filosofía do *Software Libre*. No apartado do software no hai problemas usamos a licenza [GPL V2](https://github.com/escornabot/arduino/commit/0315f2333f7aecb6eb785dcef96e3bd342fbc536). No apartado do hardware tentamos que sexa un ***Hardware de Deseño Aberto***, publicando todo o deseño da electrónica e das pezas impresas e usando para todo a licenza [Cern Open Hardware License version 2](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2).
