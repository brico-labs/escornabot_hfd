---
title: Taller Escornabot 2021
subtitle: Software y Hardware Libres
author:
- Bricolabs
tags: [Robótica, Escornabot]
date: octubre-2021
lang: es-ES
abstract: |
    Algunas aclaraciones al respecto del software y hardware libres
---

La asociación Bricolabs tiene como uno de sus objetivos principales la difusión del software y hardware libres. En este documento intentamos dar una introducción a esos conceptos.

# Software Libre

El *Software Libre* es algo muy concreto: un software es *Software Libre* si y solo si garantiza **al usuario final** las ___Cuatro Libertades___:

0. Ejecutar el programa como se desee y con cualquier propósito
1. Estudiar cómo funciona el programa y modificarlo para que haga lo que se desee
2. Redistribuir copias para ayudar a otros
3. Redistribuir copias modificadas para terceros

A veces, de forma resumida, se habla de las libertades de **Uso, Modificación y Copia**.

La definición del *Software Libre* se centra en los derechos del usuario que no pueden ser limitados por el programador o el distribuidor, en cambio en el software privativo normalmente se establecen límites a lo que el usuario puede hacer con ese software.

Las libertades 1 y 3 implican que hay que dar al usuario acceso completo al código fuente.

Podemos hablar de diversas licencias de software y de cuanto se acercan o alejan a la definición del *Software Libre*, pero si no cumplen la condición de garantizar ***Las Cuatro Libertades*** no son *Software Libre*

Específicamente, el *Open Source Software* (OSS o Software de Código Abierto en español), no es *Software Libre*.

## Uso comercial

En inglés *Free Software* puede significar *Software Gratis* o *Software Libre*, esto ha provocado todo tipo de confusiones y malentendidos. De hecho ahora se tiende a usar la expresión ***Free Libre Software*** en lugar de *Free Software* para que no queden dudas de que se trata de *Free as in Freedom*. En castellano no tenemos ese problema, pero recordemos que *Software Libre* no tiene por que ser software gratuito, y que tampoco todo el software gratis es *Software Libre*.

En realidad ***Las Cuatro Libertades*** garantizan el Uso Comercial. Las libertades 2 y 3 permiten la redistribución de cualquier Software Libre, y nadie impide cobrar por ello.

Ahora bien, para que el software que redistribuyes sea *Software Libre* no puedes limitar de ninguna manera ***Las Cuatro Libertades***.

## Software Complejo

Lo normal es que el software se componga de muchas partes: bibliotecas (o librerias), sistemas operativos, programas de utilidad etc. etc.

El conjunto del software será libre si y solo si todos sus componentes son *Software Libre*

## Copyleft

El Copyleft es un método para exigir que las versiones modificadas y/o extendidas de un *Software Libre* sean a su vez *Software Libre*; es decir se exige que cualquiera que redistribuya el software, con o sin cambios, garantice a los receptores ***Las Cuatro Libertades***.

Un software, o más bien una licencia, que especifica que las copias modificadas no son libres (limitando de cualquier forma ***Las Cuatro Libertades***) no es una Licencia Libre, no es *Software Libre*

Si la licencia exige que las versiones redistribuidas sean a su vez libres, entonces si se puede considerar *Software Libre*.

# Hardware Libre

En principio parece deseable (al menos para mucha gente, incluidos nosotros) extender la definición del *Software Libre* al Hardware. El problema es que el software es una "propiedad intelectual" casi pura. Tanto el diseño (código fuente) como el producto final (ejecutable) de un software son fáciles de copiar y distribuir, prácticamente sin coste.

El hardware no es así en absoluto, siempre hay costes asociados que además pueden ser muy grandes. Esta característica por sí sola ya impediría usar la definición de *Software Libre* para el *Hardware Libre*.

Lo que si parece transladable es la filosofía del *Software Libre*. Por ejemplo, no sería difícil aplicar la filosofía y definición del *Software Libre* a los diseños del Hardware.

Dada la naturaleza del hardware y sus problemas asociados veremos que hay varias líneas de trabajo en la definición del *Hardware Libre*. Además todavía no se avanza en todos los campos por igual, aunque hay proyectos en distintas áreas de la tecnología, la mayor parte de los intentos de definición de hardware libre y de implementación de proyectos libres se centra o está relacionado con la electrónica.

## Dispositivos de lógica programable

En el hardware además de los problemas de costes asociados que hemos mencionado, normalmente estamos hablando de sistemas complejos (análogos al software complejo que también mencionamos) Pensemos en todos los componentes que necesitamos para una impresora 3D o un automóvil por citar un proyecto más ambicioso. 

Tomemos un ejemplo de electrónica y además muy conocido: la placa Arduino. Es un ejemplo típico de Hardware Libre, pero el microprocesador ATMega que se usa para implementar el Arduino no es, de ninguna manera, libre. De la misma forma una impresora 3D usa motores, servos, etc que a su vez no son hardware libre. Si pensamos en un automovil tendríamos patentes incluso para distintos tipos de acero.

En lo que respecta a la electrónica, en los últimos años hemos visto grandes avances en la definición de dispositivos de lógica programable **libres**, ahí tenemos por ejemplo la FPGA Alhambra (ahora ya hay más) o el software de programación Icestudio. Con una FPGA sería posible diseñar una placa con las mismas funcionalidades que un Arduino, o que una tarjeta gráfica de ordenador, o incluso el procesador del propio ordenador, por citar algunas posibilidades.

Estos avances hacen posible el desarrollo de dispositivos electrónicos que serían completamente libres, en el sentido de no tener que usar necesariamente componentes cerrados.

## Open Hardware

Igual que surge el Open Source, como un intento de adoptar el ciclo de desarrollo del *Software Libre* para el software privativo. También se habla de *Open Hardware* y cada vez hay más interés (y presión) por parte de la industria para definir casos de *Open Hardware*. Hoy en día ya se despliegan *Software Defined Networks* (SDN) por parte de los operadores de telecomunicaciones y proveedores de servicios en la nube. Ahí interesa tener el hardware: servidores, routers, estaciones base de telefonía, etc. etc. definidos como Open Hardware, de manera que la red no dependa de un solo fabricante, se bajen costes, haya repuestos compatibles, etc. etc.

Es debatible hasta qué punto estos movimientos de la industria encajan con la filosofía del *Software Libre* de anteponer a todo los derechos del usuario final. Pero en cualquier caso la tendencia existe, y también puede suponer una oportunidad para el *Hardware Libre*.


# Proyecto Escornabot

En el proyecto Escornabot intentamos ceñirnos lo más posible a la filosofía del *Software Libre*. En el apartado del software no hay problemas, usamos la licencia [GPL V2](https://github.com/escornabot/arduino/commit/0315f2333f7aecb6eb785dcef96e3bd342fbc536). En el apartado del hardware intentamos que sea un ***Hardware de Diseño Abierto***, publicando todo el diseño de la electrónica y las piezas impresas y usando para todo la licencia [Cern Open Hardware License version 2](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2).
