# Herramientas

## Herramientas para soldadura de PCB

![herramientas_soldadura.jpg](img/herramientas_soldadura.jpg){ width=75%}

__El mínimo imprescindible__

* Soldador con su soporte
* Tu material preferido para limpiar la punta del soldador (estropajo metálico en la foto)
* Estaño 
* Alicates de corte
* __Gafas de protección__
* Polímetro

__Materíal adicional__

Siempre viene bien disponer de herramientas para retirar el estaño si nos equivocamos

* Aspirador de estaño
* Malla de desoldar
* Flux (también puede ser útil para soldar)

Si quieres que tu placa quede impecable y limpita:

* Alcohol isopropílico
* Cepillo de dientes
