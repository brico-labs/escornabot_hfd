# Carga de firmware Escornabot

**ACTUALMENTE LA PLACA ESCORNABOT-HFD SE ENTREGA CON EL FIRMWARE YA CARGADO**

Tendrás que seguir estas instrucciones si necesitas modificar o volver a cargar el firmware en tu robot.


## Requisitos

- Placa Escornabot HFD
- Ordenador PC con sistema GNU/Linux
- Arduino IDE (descargable de Internet)
- Firmware Escornabot (descargable de Internet)
- Cable micro-USB

##  Instalar Arduino IDE

1. Vaya a la [página oficial de Arduino](https://www.arduino.cc/), en el menú _Software > Downloads_ 

   ![descargar_arduino_ide.png](img/descargar_arduino_ide.png) \

2. Seleccione la version del Arduino IDE para su sistema operativo (nota: en los siguientes pasos de esta guía se describirá la instalación con sistema operativo libre GNU/Linux).

   ![download_arduino_so.png](img/download_arduino_so.png) \

3. Guarde el fichero descargado en una ruta de su sistema de ficheros en la que tenga acceso, por ejemplo `/tmp`

   ![download_linux64.png](img/download_linux64.png) \

4. Ejecute los siguientes comandos (ajuste los nombres de las rutas a su caso):

   ```
   cd /tmp
   tar xavf arduino-1.8.13-linux64.tar.xz
   cd arduino-1.8.13
   sudo ./install.sh
   arduino
   ``` 

 Mantenga el Arduino IDE abierto como aplicación de fondo ya que se usará más adelante (ver Carga del firmware)

![arduino_ide.png](img/arduino_ide.png) \

## Obtención del firmware Escornabot

1. Vaya la [página de descargas de Escornabot en Github](https://github.com/escornabot/arduino/releases) y descarge la última versión disponible (1.6.2 en esta guía).

   ![github_escornabot.png](img/github_escornabot.png) \

2. Guarde el fichero descargado en una ruta de su sistema de ficheros en la que tenga acceso, por ejemplo `/tmp`

3. Ejecute los siguientes comandos (ajuste los nombres de las rutas a su caso):

   ```
   cd /tmp
   tar xavf arduino-1.6.2.tar.gz
   mv arduino-1.6.2 firmware
   ```

## Carga del firmware en la PCB

1. Desde el IDE de Arduino (abierto en el paso 1.5), vaya por el menú "Archivo" ("File"), opción "Abrir" ("Open") y vaya al directorio donde haya descargado el firmware en el paso anterior (`/tmp/firmware` en esta guía).

   ![arduino_file_open.png](img/arduino_file_open.png) \

2. Abra el fichero `Escornabot.ino`

   ![escornabot_ino.png](img/escornabot_ino.png) \

3. Vaya por el menú "Herramientas" ("Tools"), opción "Placa" ("Board") y seleccione "Arduino Nano"

   ![arduino_board_nano.png](img/arduino_board_nano.png) \

4. Conecte la placa de Escornabot HFD a su PC con el cable micro-USB y vaya por el menú "Herramientas" ("Tools"), opción "Puerto" ("Port") y seleccione el puerto donde tiene conectado el Escornabot (*/dev/ttyUSB0* en esta guía)

   ![arduino_port.png](img/arduino_port.png) \

5. Vaya por el menú "Programa" ("Sketch"), y seleccione la opción "Subir" ("Upload"). Si todo ha ido bien, en la parte inferior de la pantalla debe ver un mensaje que pone "Subido" ("Done uploading").

   ![arduino_done_uploading.png](img/arduino_done_uploading.png) \

6. Compruebe que el programa se ejecuta en la placa Escornabot HFD pulsando el botón central (como primera pulsación nada más arrancar): debería ver y escuchar como la PCB ejecuta el "saludo Escornabot".

