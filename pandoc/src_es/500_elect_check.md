# Comprobaciones Eléctricas

Antes de nada haremos una comprobación visual, comprobando que no hay cortocircuitos o soldaduras incorrectas en la placa.

Usando un polímetro con fondo de escala a 200KΩ o más comprobaremos las conexiones de resistencias y pulsadores.

1) **Comprobación de la sección el teclado**
    * Conectamos la punta de prueba positiva (roja) al PAD A7, y la punta de prueba de tierra (negra) a la tierra de la placa (GND)
        * Sin pulsar ningún botón el polímetro debe indicar infinito (circuito abierto)
        * Al apretar el botón verde indicará 62KΩ (aproximadamente)
        * Botón blanco corresponde a 40KΩ
        * Botón naranja corresponde a 30KΩ
        * Botón rojo corresponde a 20KΩ
        * Botón azul corresponde a 10KΩ

        <br>
    * Con la punta de prueba roja en A7 y la negra en Vcc, el polímetro debe marcar 10KΩ

2) **Comprobación de la sección de comunicaciones**
    * Con la punta roja en Tx y la negra en GND, deberá marcar 28KΩ

En el caso de que nos falle alguna de las medidas anteriores debemos volver a repasar cuidadosamente los valores de los componentes, el estado de las soldaduras y la existencia de cortocircuitos.
