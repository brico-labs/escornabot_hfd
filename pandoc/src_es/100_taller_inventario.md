# Inventario

![materiales.jpg](img/materiales.jpg){ width=75% }

## Motores
   Dos motores de pasos (_steppers_)

## Porta baterías
   La placa admite diversas configuraciones de baterias.

## Placa Escornabot HFD
   La nueva PCB Escornabot HFD

## Zócalo y chip ULN2803
   El chip ULN2803 con su zócalo correspondiente DIP-18

## Tiras de pines
   Necesitamos
   2 x pines macho con puente
   4 x hembras tipo dupont

## Interruptor
   1 x interruptor

## Pulsadores
   5 x pulsadores

## Diodo Schottky
   1 x

## Fusible rearmable
   1 x

## Condensadores
   1 x 100 nF

## Resistencias
   4 x 1K (Marron, negro, rojo)  Posiciones R8, R9, R10 y R11
   5 x 10K (Marrón, negro, naranja) Posiciones R14, R15, R16, R17 y R19
   1 x 22K (Rojo, rojo, naranja) Posición R18

## Diodos LED
   4 x LED 3mm, LED1 (azul), LED2 (rojo), LED3 (amarillo), LED4 (verde)

## Zumbador
   1 x Buzzer Posición SG1

## Conector alimentación
   Posición SS14

## Conectores para motor
   Posiciones 'Motor Left' y 'Motor Right'
