# Soldadura de componentes en la PCB

## Preparación

Una vez más: __USA GAFAS DE SEGURIDAD__ El estaño fundido ronda los 250ºC. Una gota de estaño fundido puede causar lesiones graves en los ojos

Los soldadores están __calientes__, presta atención para no quemarte

**Ventila correctamente** la zona de soldadura para evitar inhalar los gases

**Limpia el soldador** muy a menudo, un soldador limpio transmite bien el calor.

## Procedimiento

En toda la descripción vamos a mantener la placa de circuito impreso (PCB) en una posición que denominaremos "natural", por darle un nombre. En las fotos se puede ver que está apaisada, con los conectores de los motores a la derecha y el interruptor a la izquierda.

Soldar es fácil. Pero si nunca soldaste antes es una buena idea que le eches antes un vistazo a [**un manual como este**](https://mightyohm.com/files/soldercomic/translations/Comic_Soldar_Es_Facil_Spanish_Final.pdf) y que practiques un poco soldando unos componentes en una placa de puntos, o mejor, que montes un pequeño kit de iniciación a la soldadura. Es importante que compruebes dos veces cada paso antes de soldar, porque corregir los errores suele ser truculento.

Es aconsejable soldar los componentes con perfil más bajo primero (los que menos altura tienen sobre el plano de la PCB) y luego seguir con los más altos. Esto nos permite que los componentes no se muevan ni caigan cuando apoyamos la placa contra la mesa.

1) Soldadura de resistencias
    * Soldaremos primero todas las resistencias
    * Es una buena idea colocar todas las resistencias de forma que se lea su valor comodamente. Es cómodo cuando revisemos la placa y quizá más estético. Con la placa en la posición de las fotos deberíamos insertar las resistencias con la banda de tolerancia (color dorado o plateado) a la derecha.
    * __Asegúrate de colocar cada resistencia en su lugar correspondiente__. Hay tres valores diferentes de resistencias. Los colores que indicamos se leen de izquierda a derecha, dejando siempre la franja dorada o plateada a la derecha.
        * 4 x 1K (marrón, negro, rojo)  R8, R9, R10 y R11 
        * 5 x 10K (marrón, negro, naranja) R14, R15, R16, R17 y R19 
          __NO SOLDAR R12__
        * 1 X 22K (rojo, rojo, naranja) R18
    * Un buen truco es soldar solo una de las patillas de cada resistencia en una primera vuelta. A continuación comprobar si quedaron bien sujetas y pegadas a la placa de circuito impreso. En caso contrario, se puede corregir la posición sujetando o empujando la resistencia con una herramienta o apoyándola sobre la mesa y calentando el terminal soldado. **NO TE QUEMES**. Una vez comprobado que todas las resistencias están en su sitio puedes soldar la otra patilla de cada una.
    * Comprueba que cada soldadura tiene suficiente estaño sin que sobre y que es brillante (que no es una soldadura fría).
   
    ![resistencias.jpg](img/resistencias.jpg){ width=35% } \

    
2) Soldar el diodo Schottky
   * __ATENCIÓN__ El diodo es un componente con polaridad, hay que insertarlo en la PCB en la posición correcta
   * El __cátodo__ del diodo viene marcado con una banda plateada en el propio dispositivo y con cinta roja en la tira de componentes (es posible que recibas el diodo sin la tira de papel de color rojo)
   * El __cátodo__ del diodo en la posición "natural" de la placa queda hacia la izquierda
   
   ![diodo.jpg](img/diodo.jpg){ width=35% } \

3) Soldar el zócalo DIP-18 del ULN2803
    * __ATENCIÓN__ el zócalo (y el propio chip ULN2803) también deben ser montados en un sentido determinado. La muesca del zócalo debe coincidir con la muesca en el *silkscreen* de la PCB (hacia arriba en la posición "natural" de la PCB)
    * Podéis usar los conectores de los motores o cualquier otro objeto para apoyar la parte contraria de la placa y conseguir así que la placa esté horizontal y el zócalo se pegue bien a la PCB para soldarlo
    * Un buen truco es soldar sólo dos patillas diagonalmente opuestas del zócalo. Comprueba que el zócalo queda totalmente pegado a la PCB y ajusta la posición si hace falta. Una vez en su sitio es el momento de soldar las otras 14 patillas. Comprueba que no saltaste ninguna y que no haya patas adyacente unidas entre si por puentes de estaño
    
   ![zocalo.jpg](img/zocalo.jpg){ width=35% } \
    
4) Soldar el interruptor
   * El interruptor hay que soldarlo de forma que el mando conmutador apunte hacia fuera de la placa para poder accionarlo fácilmente
   * Es aconsejable soldar una pata, comprobar la posición y luego soldar los demás terminales.
   
   ![switch.jpg](img/switch.jpg){ width=35% } \

5) Condensador 
    * El condensador "de lenteja" de 100 nF no tiene polaridad, pero es aconsejable soldarlo de forma que el valor rotulado quede visible, es decir el valor debe ir hacia abajo, en la cara del condensador que quede opuesta al zócalo DIP-18. 
    * No debes confundir el condensador con el fusible, aunque son muy parecidos externamente. En caso de duda puedes medir la resistencia entre patas con el polímetro: el condersador tiene una resistencia muy alta, infinita; el fusible tiene una resistencia muy baja, de 0 ohmios, o casi.
    
   ![condensador.jpg](img/condensador.jpg){ width=35% } \
    
5) Diodos LED
    * Como todos los diodos, **los LED tienen polaridad**. El cátodo de un diodo LED suele marcarse con una patilla más corta. Además, la planta circular del diódo también está aplanada por el ládo del cátodo. Y si te fijas bien, podrás observar el cátodo a simple vista dentro del propio diodo 
    * Esa muesca plana se refleja también en el *silkscreen* de los diodos.

    ![detalle_catodo_leds.png](img/detalle_catodo_leds.png){.align-left width=25%} 

      Se aprecia más fácilmente en los diodos izquierdo y derecho (siempre con respecto a la posición que hemos tomado en las imágenes para soldar la placa) Con la PCB en nuestra posición, los cátodos de los cuatro diodos LED van en el orificio superior. 

    * La posición de los colores de los LED de los escornabots está estandarizada según el reglamento internacional de señalización de naves, asi que tenemos:
        * **LED1** azul
        * **LED2** rojo
        * **LED3** amarillo
        * **LED4** verde
    <br>
    
   ![leds.jpg](img/leds.jpg){ width=35% } \

    
6) Botones
    * Los botones no tienen polaridad, pero sólo se pueden poner en dos posiciones, ya que sus patillas no forman un "cuadrado" sino un "rectangulo". Comprueba que las patillas coinciden con los orificios e insertalos hasta que queden correctamente pegados a la PCB. Quizá tengas que hacer un poco de fuerza.
    * Los botones soportan bastante esfuerzo mecánico en el uso normal del Escornabot. Para fijarlos vas a necesitar más estaño que en las resistencias
    
    ![botones.jpg](img/botones.jpg){ width=35% } \
    
7) Soldar los conectores de motores
    * Las dos pequeñas muescas que tiene cada uno de los dos conectores de los motores deben quedar orientadas hacia el exterior de la placa, hacia la derecha.
    * Tendrás que hacer un poquito de fuerza para insertar los conectores hasta el fondo de forma que queden pegados a la PCB antes de soldarlos.
    
8) Soldar el zumbador
    * **ATENCIÓN** el zumbador tiene polaridad, el polo positivo del zumbador debe coincidir con el polo positivo indicado en el *silkscreen* en la PCB. Quizá tengas que doblar un poco las patas y abrirlas para que entren en los agujeros correspondientes de la PCB. A lo mejor no queda totalmente pegado a la placa.
    
   ![zumba.jpg](img/zumba.jpg){ width=35% } \
    
9) Soldar tira de 2 x pin macho (con puente)
    * Son más fáciles de apoyar con el puente puesto
    * Los extremos cortos son los que se insertan en la PCB
    * En este caso también es útil soldar sólo una de las patas, comprobar la posición y luego soldar la otra.

    ![tirax2.jpg](img/tirax2.jpg){ width=35% } \
    
10) Conector de alimentación
    * Las entradas para los cables deben quedar apuntando hacia abajo, hacia afuera de la PCB
    * Recuerda que el **positivo de la pila debe** conectarse al terminal de la **izquierda** y **el negativo al de la derecha**
    
    ![con_alimentacion.jpg](img/con_alimentacion.jpg){ width=35% } \
    
11) Tira de 4 x pines hembra
    * Esta tira de conectores permitirá conectar un modulo bluetooth
    * Se sueldan justo a lado del condensador de lenteja
    * Procura dejar la tira bien pegada a la PCB y vertical, es mejor soldar solo un pin, comprobar la posición y corregirla sies necesario, para luego soldar los otros tres terminales.
    
    ![tirax4.jpg](img/tirax4.jpg){ width=35% } \
    
12) Soldar el fusible rearmable
    * No tiene polaridad, por lo que se puede montar en cualquier posición. 
    * Cuando las pilas se ponen al reveés se calienta, por lo que la forma de las patas asegura que quede un poco separado de la placa y que circule el aire a su alrededor.

    ![fusible.jpg](img/fusible.jpg){ width=35% } \
    
13) Insertar el chip ULN

    **IMPORTANTE**: la muesca del chip debe coincidir con la muesca del zócalo y la serigrafía de la placa.

14) Colocar las capuchas de la botonera

    * Los colores de los capuchones deberían coincidir con los de los respectivos LED. 
    * Se colocan presionandolos sobre cada botón. 
    * El capuchón redondo va en el botón del centro

15) Acabado final

    Si tenéis alcohol isopropílico es el momento de limpiar la cara de las soldaduras de vuestra placa con un poco de alcohol y un cepillo suave (un cepillo de dientes por ejemplo)

**Resultado final del montaje de la PCB:**


![final_pcb.jpg](img/final_pcb.jpg) \

    
## Checklist
* __Usa gafas de seguridad__
* Asegura la ventilación
* Evita materiales inflamables en la zona de soldadura
* Limpia el soldador a menudo
* Orden aconsejado de soldadura
    * Resistencias
    * Diodo Schottky
    * Zócalo DIP-18
    * Interruptor
    * Condensador de 100 nF
    * Diodos LED
    * Botones
    * Conectores de motores
    * Zumbador
    * Tira 2 x pin macho (con puente)
    * Conector de alimentación
    * Tira de 4 x pines hembra
    * Fusible
    * Insertar el chip ULN
    * Colocar capuchones de botonera
