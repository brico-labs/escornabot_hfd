# Montaje del robot

## Material necesario

* La PCB HFD con todos sus componentes ya soldados
* 2 x motores de pasos (steppers)
* Las seis piezas impresas (ver foto) Las piezas están disponibles [en este github](https://github.com/rafacouto/3d-escornabot/). De los tres chasis Escorna CPU disponibles en el github **sólo necesitas el primero: EscornaCPU 2.10-2.12 bracket escornacpu-2_10-bracket.stl**
* 16 x tornillos M3x10 (nosotros solemos usar tornillos para llave Allen (hexagonal) conocidos como DIN912.
* 2 x tuercas M3
* La llave para apretar los tornillos

## Material adicional

Este material no es imprescindible pero facilita mucho la tarea

* Soldador o alguna forma de calentar las tuercas
* Un escariador o una broca de 3mm para repasar la boca de los orificios para tornillos en las piezas impresas en 3D

![piezas.jpg](img/piezas.jpg){ width=50% }

## Montaje

Para proceder al montaje de todas las piezas de impresión 3D y la PCB conviene repasar previamente todos los orificios de los tornillos de todas las piezas con alguna herramienta que nos permita ensanchar ligeramente el orificio del tornillo. Un destornillador o un *cutter* pequeño nos puede valer perfectamente.

![Quitando rebabas](img/escariador.jpg){ width=50% }

También es muy recomendable insertar los tornillos en sus orificios con las piezas separadas y darles un par de vueltas con la llave (¡no más!) para que dejen la rosca marcada en el orificio, eso hará que insertar los tornillos más tarde sea muy fácil.

### Pasos del montaje

El primer paso es montar los motores en su soporte

![Soporte de motores](img/porta_motores_a.jpg){ width=50% } \

Asegurate de que los cableados de los motores salgan por el frente del portamotores. Es decir, del lado en que en orificio del tornillo es más grande para permitr que el tornillo quede encastrado.

En la foto siguiente podrás ver los motores ya montados.

Una vez fijados los motores al portamotores, debemos unir el portamotores con el arco del chasis (que es la pieza que soportará el portapilas)

![Motores y Arco del Chasis](img/porta_motores_b.jpg){ width=50% } \

Tras unir el portamotores al arco del chasis, les damos la vuelta para proceder a atornillar el soporte de la bola loca.

![Chasis (vista inferior)](img/chasis_a.jpg){ width=50% } \

El soporte de la bola loca cerrará el arco del chasis con el encastre de la bola en la parte trasera del robot

![Soporte de la bola](img/chasis_b.jpg){ width=50% } \

Volvemos a dar la vuelta a nuestro Escornabot para fijar el porta-PCB

![Chasis (vista superior)](img/chasis_c.jpg){ width=50% } \

El porta-PCB debe orientarse con los brazos asimétricos hacia el frente del robot

![Soporte de la PCB](img/chasis_d.jpg){ width=50% } \

Y procedemos ya a atornillar la PCB al chasis del robot. 

![Soporte de PCB (vista superior)](img/porta_pcb.jpg){ .alignleft width=50% } ![PCB en su soporte](img/porta_pcb_b.jpg){ width=50% }

Ahora debemos insertar las tuercas M3 en sus posiciones dentro del cubo de las ruedas. Nos será de gran ayuda un soldador, alicates finos y alguna herramienta que nos permita ejercer presión en las tuercas para insertarlas en su sitio, alineadas con el orificio del tornillo.

![Tuerca de la rueda](img/rueda_tuerca.jpg){ width=50% } \

Una vez colocadas las tuercas no tenemos más que fijarlas a los ejes de los motores con un tornillo cada una. Hay que asegurarse de que el tornillo hace tope contra el rebaje plano del eje.

Conectamos motores (asegúrate de que los conectores entran en la posición, y orientación correcta)

![Escornabot Terminado](img/escornabot.jpg){ width=50% } \
