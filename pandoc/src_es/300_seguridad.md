# Seguridad

## covid-19

Aplican todas las medidas de seguridad que hayan dictado las autoridades sanitarias en la zona donde realices el montaje.

Si haces el montaje en un espacio colectivo, en ausencia de indicaciones concretas de seguridad, te aconsejamos que:

* Debes usar mascarilla en todo momento
* Mantén la distancia de seguridad de al menos metro y medio
* Lávar las manos al entrar en el museo y antes de salir. Puedes lavarte siempre que lo creas necesario durante el taller
* Las placas PCB y resto de materiales se desinfectan en una mesa apartada de la zona de soldadura
* El lavado de placas con alcohol isopropílico también se hace en esta mesa lejos de los soldadores

## Medidas de seguridad soldando

* La parte metálica del soldador está caliente (puede llegar fácilmente a 300ºC) ¡No la toques!
* Usa siempre gafas de seguridad para soldar, pueden saltar gotas de estaño fundido
* Suelda siempre en un local amplio y bien ventilado, __evita inhalar los gases que desprende el estaño fundido__
* Sujeta las patillas de los componentes con una mano y cortalas con la otra, __evita que las patillas salten__
* **No tengas material inflamable** como el alcoholo isopropílico o sanitario o el gel hidroalcohólico, **en la zona de soldadura**. Manten zonas separadas para soldadura por un lado y limpieza y desinfección por el otro
