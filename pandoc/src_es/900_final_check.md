# Comprobaciones Finales

Con el robot completamente montado y con el  bootloader y el firmware cargados, el robot debería interpretar unha breve melodía al conectarlo (el  saludo  escornabótico), y responder a las teclas con un pitido. Al pulsar una pequeña secuencia de teclas de dirección y después el botón redondo el robot debería moverse. En el caso de que esto no ocurra revisa el apartado [Comprobaciones Eléctricas]

## Axustes del movimiento

En algunos casos el escornabot se mueve al revés de lo esperado. Al pulsar la tecla azul y luego el botón redondo, el robot debería responder con un movimiento hacia donde están los conectores de los motores (frente).

En el caso de que se mueva en dirección contraria, según nuestra experiencia, será debido a que algunos motores funcionan en una dirección y otros en la contraria, sin que haya ninguna referencia que nos permita distinguir unos de otros. Para solucionarlo vamos a volver a cargar el  firmware después de hacer un pequeño cambio.

- Tendrás que editar el fichero `Configuration.h`. Puedes usar el IDE de Arduino abriendo el fichero como se indica en la sección [Carga del firmware  Escornabot] o con un procesador de texto básico que te permita editarlo sin cambiar la extensión `.h`
- Alrededor de la línea 100 del código encontrarás el texto: `#define  STEPPERS_ ROTATION 0` que debes cambiar por este otro `#define  STEPPERS_ ROTATION 1`
- Guarda el fichero `Configuration.h`
- Carga el nuevo *firmware* de acuerdo con las instrucciónes del apartado [Carga de firmware  Escornabot].

**GOZA DE TU NUEVO ROBOT**

<BR>
En el caso de que no se corrija el problema o que se produzca algún otro comportamiento extraño debes ponerte en contacto con nosotros.

