# Carga de bootloader Optiboot 

**ACTUALMENTE LA PLACA ESCORNABOT-HFD SE ENTREGA CON EL BOOTLOADER YA CARGADO**

Estas instrucciones serán útiles si quieres cambiar o se corrompe el bootloader, o si por cualquier otro motivo es necesario volver a cargarlo.

También es posible hacer la carga del bootloader desde la aplicación ARDUINO, sin necesidad de un programador ICSP específico.

## Requisitos

- Placa Escornabot HFD
- Ordenador PC con sistema GNU/Linux
- Programador ICSP (USBasp)
- Utilidad AVRdude (descargable de Internet)
- Bootloader Optiboot (descargable de Internet)

![usbasp_programmer.jpg](img/usbasp_programmer.jpg)

## Instalación de AVRdude

Debe instalar el paquete avrdude desde la distribución que tenga en su PC. Por ejemplo, en sistemas Debian/Ubuntu ejecute la siguiente línea en la consola:

```
sudo apt install avrdude
```

Desde la línea de comandos de su sistema 

## Obtención del bootloader

Descarge la [versión 8 del archivo Optiboot-8.0.zip de su página de Github](https://github.com/Optiboot/optiboot/releases) y selecione "Abrir con gestor de archivos".

![optiboot_download.png](img/optiboot_download.png)

Navegue por el archivo comprimido y selecione el subdirectorio */Optiboot-8.0/bootloaders/optiboot/*. Extraiga el fichero **optiboot_atmega328.hex** a una ruta donde tenga acceso, por ejemplo */tmp*

![optiboot_binary.png](img/optiboot_binary.png)

## Carga de Optiboot en placa HFD

Conecte el programador USBasp en el puerto USB de su PC y pinche la interfaz de programación ICSP como se indica en la foto:

![icsp_interface.jpg](img/icsp_interface.jpg)

Ejecute la siguiente orden desde la consola y espere unos segundos a que AVRdude cargue el bootloader en la memoria del microcontrolador:

```
avrdude -c usbasp -p m328p -U flash:w:/tmp/optiboot_atmega328.hex -U lfuse:w:0xF7:m -U hfuse:w:0xDE:m -U efuse:w:0xFF:m -U lock:w:0x2F:m
```



