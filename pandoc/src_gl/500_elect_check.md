# Comprobacións Iniciais

Primeiro de todo faremos unha comprobación visual, comprobando que no hai curtocircuitos ou soldaduras incorrectas na placa. Fíxache en que están feitas e  estañadas todas as soldaduras e que non hai pontes de estaño unindo pistas próximas. Revisa sobre todo as zonas marcadas na imaxe da PCB vista desde abaixo, e na zona do controlador de motores, xa que ten moitas patas próximas.

![posibles cortocircuitos](img/cortocircuitos.jpg){ width=50% }

Comproba tamén que  o circuíto integrado de control de motores e que o  diodo estean ben orientados, como indican as fotos. En caso de dúbida revisa a sección de soldadura da placa. 

## Polaridade das Pilas

Lembra que coa placa na "posición natural", o **positivo** da pila debe conectarse ao terminal da **esquerda** e o **negativo ao da dereita**. En caso de dúbida comproba cun  polímetro que a alimentación está ben.

## Comprobacións Eléctricas

Usando un polímetro co fondo de escala a 200KΩ ou máis comprobaremos as conexións das resistencias e dos pulsadores.

1) **Comprobación da sección do teclado**
    * Conectamos a punta de proba positiva (vermella) ao **PAD A7**, e a punta de proba de terra (negra) á terra da placa (**GND**). Non hai un punto marcado explicitamente como **PAD A7** pero pódelo identificar facilmente na seguinte imaxe.

        ![identificación de pad_A7](img/pad_A7.jpg){ width=50% }
    
        * Sen pulsar ningún botón o polímetro debe indicar 10,7 KΩ aproximadamente
        * Ao apretar o botón verde indicará 9,1 KΩ (aproximadamente)
        * O botón blanco corresponde a 7,9 KΩ
        * O botón laranxa corresponde a 8,4 KΩ
        * O botón vermello corresponde a 6,9 KΩ
        * O botón azul corresponde a 5,1 KΩ
    <br>
    * Coa punta de proba vermella na A7 e a negra en Vcc, o polímetro debe marcar 10,7 KΩ

<br>

2) **Comprobación da sección de comunicacións**
    * Coa punta vermella en **Tx** e a negra en **GND**, deberá marcar 17,7 KΩ. Se non podes acceder facilmente ao terminal  Tx podes introducir un anaco dunha pata dunha resistencia no  conector.

No caso de que nos falle algunha das medidas anteriores debemos volver repasar os valores dos compoñentes, o estado das soldaduras e a existencia de curtocircuitos na PCB.

Se os valores son moi distintos, o máis probable é que haxa un fallo na soldadura das resistencias. Se o valor non cambia ao pulsar algún dos botóns, o máis probable é que ese botón teña unha soldadura fría. Se algúns valores coinciden, e outros non, tamén é posible que sexa unha das resistencias a que ten unha soldadura defectuosa

## Conexión do Escornabot

Ao acender o  escornabot debería acenderse polo menos un LED azul. Se a PCB xa ten cargado a bootloader e o firmware (ver nas seguintes seccións) debería encederse algún outro LEDs, e producirse algún son ao tocar as teclas. Se non ocorre nada diso fíxache no fusible rearmable. Cuidado: **PODE ESTAR MOI QUENTE**. Se é así significa que hai un curtocircuíto e é importante que apagues o  interruptor do robot e volvas facer as verificacións indicadas máis arriba, especialmente a polaridade das pilas, dos diodos e do controlador dos motores

