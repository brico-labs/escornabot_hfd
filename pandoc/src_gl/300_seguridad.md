# Seguridade

## covid-19

Son de aplicación todas as medidas de seguridade que ditasen as autoridades sanitarias na zona onde realices a montaxe. Se fas a montaxe nun espazo colectivo, en ausencia de indicacións específicas de seguridade, aconsellámosche:

* Usar a mascarilla todo o tempo
* Manter a distancia de seguridade (alo menos metro e medio)
* Lavar as mans ao entrar no museo e antes de sair. Podes lavarte sempre que queiras durante o transcurso do obradoiro.
* As placas  PCB e resto de materiais pódense  desinfectar nunha mesa apartada da zona de soldadura
* O lavado de placas con alcol isopropílico tamén se fai nesta mesa lonxe dos soldadores

## Medidas de seguridade soldando

* A parte metálica do soldador está quente (pode chegar fácilmente aos 300ºC) ¡Non a toques!
* Usa sempre lentes de seguridade para soldar, poden saltar gotas de estaño fundido
* Solda sempre nun local amplio e ben ventilado, __evita inhalar os gases que desprende o estaño fundido__
* A aliaxe de metais que usas para  soldar impregna un pouco os dedos. Lava as mans con auga e xabón cando acabes de  soldar para eliminalo. Non toques alimentos mentres estás  soldando. En xeral non é necesario usar luvas nin outras proteccións adicionais para  soldar.
* __Evita que as  patillas dos compoñentes salten cando as curtas__. Poden facer dano! Para iso suxeita as  patillas dos compoñentes cunha man e  cortalas coa outra.
* **No teñas material inflamable** como o alcohol isopropílico ou sanitario, ou o xel hidroalcohólico **na zona de soldadura**. Manten zonas separadas para soldadura por unha banda e limpeza e desinfección pola outra.
