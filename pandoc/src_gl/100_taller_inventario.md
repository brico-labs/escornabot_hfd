# Inventario

![Materiais](img/materiales.jpg){ width=75% }

## Motores
   - Dous motores de pasos (_steppers_)

## Porta baterías
   - A placa admite diversas configuracións de baterias.

## Placa Escornabot HFD
   - A nova PCB Escornabot HFD

## Zócalo y chip ULN2803
   - O chip ULN2803 con seu correspondiente zócalo DIP-18

## Tiras de pins
   Necesitamos
   - Unha tira de 2 x pins macho con ponte
   - Unha tira de 4 x femias tipo dupont

## Interruptor
   - 1 x interruptor

## Pulsadores
   - 5 x pulsadores

## Diodo Schottky
   - 1 x Diodo

## Fusible rearmable
   - 1 x Fusible

## Condensadores
   - 1 x 100 nF

## Resistencias
   - 4 x 1K (Marron, Negro, Vermello)  Posicións R8, R9, R10 e R11
   - 5 x 10K (Marrón, Negro, Laranxa) Posicións R14, R15, R16, R17 e R19
   - 1 x 22K (Vermello, Vermello, Laranxa) Posición R18

## Diodos LED
   - 4 x LED 3mm: LED1 (azul), LED2 (rojo), LED3 (amarillo), LED4 (verde)

## Zumbador
   - 1 x Buzzer Posición SG1

## Conector alimentación
   - Posición SS14

## Conectores para motor
   - Nas posicións 'Motor Left' e 'Motor Right'
