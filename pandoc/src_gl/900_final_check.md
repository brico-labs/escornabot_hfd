# Comprobacións Finais

Co robot completamente montado e co  bootloader e  firmware cargados, o robot debería interpretar unha breve melodía ao conectalo (o saúdo  escornabótico), e responder as teclas cun asubío. Ao pulsar unha pequena secuencia de teclas de dirección e logo o botón redondo o robot debería moverse. No caso de que isto non ocorra revisa as apartado [Comprobacións Eléctricas]

## Axustes no movemento
Nalgúns casos o  escornabot móvese ao revés do esperado. Ao pulsar a tecla azul e logo o botón redondo, o robot debería responder cun movemento cara a onde están os conectores dos motores.

No caso de que se mova en dirección contraria é debido a que algúns motores funcionan nunha dirección e outros na contraria, sen que haxa ningunha forma nin referencia que permita diferencialos. Para solucionalo imos volver cargar o  firmware despois de facer un pequeno cambio.

- Vas ter que editar o ficheiro `Configuration.h`. Podes usar o IDE de arduino abrindo o ficheiro como se indica na sección [Carga do firmware  Escornabot] ou cun procesador de texto básico que che permita editalo sen cambiar a extensión `.h`
- Ao redor da liña 100 do código atoparás o texto: 
  
  `#define  STEPPERS_ ROTATION 0` 
  
  que debes cambiar por este outro:
  
  `#define  STEPPERS_ ROTATION 1`
- Garda o ficheiro `Configuration.h` 
- Carga o novo firmware de acordo ás instrucións do apartado [Carga do firmware  Escornabot]

**GOZA DO TEU NOVO ROBOT**

<BR>
No caso de que non se corrixa o problema ou que se produza algún outro comportamento estraño debes poñerche en contacto connosco.

