# Soldadura de compoñentes na PCB

## Preparación

Unha vez mais: __USA LENTES DE SEGURIDADE__ O estaño fundido ronda os 300ºC. Unha pinga de estaño fundido pode causar lesións graves nos ollos

Os soldadores están __quentes__, presta atención para non queimarte

**Ventila correctamente** a zona de soldadura para evitar inhalar os gases

**Limpa o soldador** frecuentemente, un soldador limpo transmite mellor a calor.

## Procedemento

En toda a descrición imos manter a placa de circuíto impreso (PCB) nunha posición que chamaremos "natural", por darlle un nome. Nas fotos pódese ver que está apaisada, cos conetores dos motores á dereita e o interruptor á esquerda.

Soldar é fácil. Pero se nunca soldaches antes é unha boa idea que lle botes antes unha ollada a [**un manual coma este**](https://mightyohm.com/files/soldercomic/translations/Comic_Soldar_Es_Facil_Spanish_Final.pdf) e que practiques un pouco  soldando uns compoñentes nunha placa de puntos, ou mellor, que montes un pequeno kit de iniciación á soldadura. 

É importante que comprobes dúas veces cada paso antes de soldar, porque **corrixir os erros adoita ser truculento**, laborioso e aburido.

É aconsellable soldar os compoñentes con perfil máis baixo primeiro (os que menos altura teñen sobre o plano da PCB) e logo seguir cos máis altos. Isto permítenos que os compoñentes non se movan nin caian cando apoiamos a placa contra a mesa.

1) Soldadura das resistencias
    * Soldaremos primeiro todas as resistencias
    * É boa idea colocar todas as resistencias de xeito que se lea o seu valor comodamente. É mais cómodo cando revisemos a placa e mesmo mais estético. Coa placa na posición das fotos deberíamos insertar as resistencias coa banda de tolerancia (color dorado ou plateado) a dereita.
    * __Mira ben de colocar cada resistencia no seu lugar correspondiente__. Hai tres valores diferentes de resistencias. As cores que indicamos lense de esquerda a dereita, deixando sempre a franxa dourada ou prateada á dereita.
        * 4 x 1K (marrón, negro, vermello)  R8, R9, R10 y R11 
        * 5 x 10K (marrón, negro, laranxa) R14, R15, R16, R17 y R19 
          __NON SOLDAR R12__
        * 1 X 22K (vermello, vermello, laranxa) R18
    * Un bo truco é soldar só unha das patillas de cada resistencia na primeira volta. A continuación comprobar se quedaron ben suxeitas e pegadas á placa de circuito impreso. En caso contrario, pódese corrixir a posición suxeitando ou empuxando a resistencia cunha ferramenta ou apoiandoa sobre da mesa e quentando o terminal soldado. **NON TE QUEIMES**. Unha vez comprobado que todas as resistencias están no seu sitio podes soldar a outra patilla de cada unha.
    * Comproba que cada soldadura ten suficiente estaño sen que sobre e que é brillante (que non é unha "soldadura fría").

   ![Resistencias](img/resistencias.jpg){ width=35% } \

    
2) Soldar o diodo Schottky
   * __ATENCIÓN__ O diodo es un compoñente con polaridade, hai que insertarlo na PCB na posición correcta
   * O __cátodo__ do diodo ven marcado cunha banda prateada no propio dispositivo e cunha cinta vermella na tira de compoñentes (é posible que recibas o  diodo sen a tira de papel de cor vermella).
   * O __cátodo__ do diodo na posición "natural" da placa queda cara a esquerda.
   
   ![Diodo](img/diodo.jpg){ width=35% } \

3) Soldar o zócalo DIP-18 do ULN2803
    * __ATENCIÓN__ o zócalo (e mais o propio chip ULN2803) tamén deben ser montados nun sentido determinado. A marca do zócalo debe coincidir coa marca no *silkscreen* da PCB (cara arriba na posición "natural" da PCB)
    * Podedes usar os conetores dos motores ou cualquer outro obxeto para apoiar a parte contraria da placa e conseguir así que a placa estea horizontal e o  zócalo péguese ben á PCB para soldalo.
    * É mellor soldar só as duas patillas diagonalmente opuestas do zócalo. Mira que o zócalo quede totalmente pegado á PCB e axusta a posición si fai falta. Unha vez no seu sitio é o momento de soldar as outras 14 patillas. Mira que non saltaches ningunha e que non haxa patas adxacente unidas entre si por pontes de estaño.

   ![Zocalo](img/zocalo.jpg){ width=35% } \
    
4) Soldar o interruptor
   * O interruptor haino que soldar de xeito que o mando conmutador mire cara afora da placa para que sexa doado accionalo
   * É aconsellable  soldar unha pata, comprobar a posición e logo  soldar os demais terminais.
   
   ![Interruptor](img/switch.jpg){ width=35% } \

5) O Condensador 
    * O condensador "de lentella" de 100 nF non ten polaridade, pero é mellor  soldalo de xeito que quede visibel o valor rotulado, isto é o valor debe quedar cara abaixo, na cara do condensador que quede oposta ao zócalo DIP-18. 
    * Non debes confundir o condensador có fusible rearmable, ainda que son moi semellantes externamente. Se tes dúbidas podes medir a resistencia entre as patas do compoñente có polímetro: o condersador ten unha resistencia moi alta, infinita; o fusible ten unha resistencia moi baixa, de 0 ohmios, ou casi.
    
    ![Condensador](img/condensador.jpg){ width=35% } \
    
5) Os Diodos LED
    * Como todos os diodos, **os LED teñen polaridade**. O cátodo dun diodo LED adoita marcarse cunha patilla mais curta. Ademais, a planta circular do diódo tamén está aplanada polo lado do cátodo. É se miras ben, poderás observar o cátodo a simple vista dentro do propio diodo 
    * Esa marca plana tamén ven marcada no *silkscreen* dos diodos na PCB.

    ![Detalle del cátodo](img/detalle_catodo_leds.png){.align-left width=25%} 

      Apreciase mais fácilmente nos diodos esquerdo e dereito (sempre con respecto á posición que tomamos nas imágenes para soldar a placa) Coa PCB na nosa posición, os cátodos dos catro diodos LED van no orificio superior. 

    * A posición das cores dos LED dos escornabots está estandarizada según o regulamento internacional de senalización de naves, asi que temos:
        * **LED1** azul
        * **LED2** rojo
        * **LED3** amarillo
        * **LED4** verde
    
   ![Os LED](img/leds.jpg){ width=35% } \

    
6) Os Botóns
    * Os  botóns non teñen polaridade, e podense poñer só en duas posicións, xa que as suas patillas no forman un "cadrado" senon un "retángulo". Comproba que as patillas coinciden cos orificios e insertaos ata que queden correctamente pegados á PCB. Poida que teñas que facer un pouquiño de forza.
    * Os botóns soportan bastante esforzo mecánico no uso normal do Escornabot, non escatimes estaño. Para fixalos vas necesitar máis estaño que nas resistencias.
    
    ![Botóns](img/botones.jpg){ width=35% } \
    
7) Soldar os conectores dos motores
    * As duas pequenas marcas que ten cada un dos dous conectores dos motores deben quedar orientadas cara ao exterior da placa, cara a dereita nas fotos.
    * Terás que facer un pouco de forza para insertar os conectores ata o fondo de xeito que queden pegados á PCB antes de soldalos.
<br>

8) Soldar o zoador
    * **ATENCIÓN** el zoador ten polaridade, o polo positivo do zoador debe coincidir có polo positivo indicado no *silkscreen* na PCB. Quizá teñas que dobrar un pouco as patas e abrilas para que entren nos buracos correspondentes da PCB. Se cadra non queda totalmente pegado á placa.
    
    ![Zoador](img/zumba.jpg){ width=35% } \
    
9) Soldar a tira de 2 x pin macho (con ponte)
    * Son mais doados de apoiar coa ponte posta
    * Os extremos curtos son os que se insertan na PCB
    * Neste caso tamén é útil soldar só unha das patas, comprobar a posición e despois soldar a outra.

    ![Tira con puente](img/tirax2.jpg){ width=35% } \
    
10) O Conector de alimentación
    * As entradas para os cables deben quedar apuntando cara abaixo, cara afora da PCB
    * Lembra que o **positivo da pila** debe conectarse no terminal da **esquerda** e **o negativo no terminal da dereita**
    
    ![Conector Alimentación](img/con_alimentacion.jpg){ width=35% } \
    
11) A Tira de 4 x pines hembra
    * Esta tira de conectores permitirá conectar un modulo bluetooth
    * Soldanse xusto ao lado do "condensador de lentella".
    * Procura deixar a tira ben pegada á PCB e vertical, é mellor soldar só un pin, comprobar a posición e corrixila se é necesario, para logo soldar os outros tres terminais.
    
    ![Tira 4xpin](img/tirax4.jpg){ width=35% } \
    
12) Soldar o fusible rearmable
    * Non ten polaridade, polo que se pode montar en calquera posición.
    * Se se poñen as pilas ao revés por accidente quéntase, polo que a forma das patas asegura que quede un pouco separado da placa e que circule o aire ao seu redor.

    ![Fusible](img/fusible.jpg){ width=35% } \
    
13) Insertar o chip ULN

    **IMPORTANTE**: a muesca do chip debe coincidir coa muesca do zócalo e a  serigrafía da placa.

14) Colocar as capuchas da botonera
    * As cores dos capuchones deben coincidir cos dos respectivos LED. 
    * Colocanse presionando sobre cada botón. 
    * O capuchón redondo vai no botón do centro

<br>

15) Acabado final

    Se tedes alcol isopropílico é o momento de limpar a cara das soldaduras da vosa placa cun pouco de alcohol e un cepillo suave (un cepillo de dentes por exemplo)

**Resultado final da montaxe da PCB:**


![PCB Escornabot](img/final_pcb.jpg) \

    
## Checklist
* __Usa lentes de seguridade__
* Asegura a ventilación
* Evita materiais inflamables na zona de soldadura
* Limpia o soldador a miúdo
* Orde aconsellada de soldadura
    * Resistencias
    * Diodo Schottky
    * Zócalo DIP-18
    * Interruptor
    * Condensador de 100 nF
    * Diodos LED
    * Botóns
    * Conectores dos motores
    * Zoador
    * Tira 2 x pin macho (con ponte)
    * Conector de alimentación
    * Tira de 4 x pines femia
    * Fusible
    * Insertar o chip ULN
    * Colocar os capuchones dos Botóns
    * Limpeza final
