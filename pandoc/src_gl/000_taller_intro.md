---
title: Taller Escornabot 2021
author:
- Bricolabs
tags: [Robótica, Escornabot]
date: octubre-2021
lang: gl-ES
abstract: |
    Instrucións de montaxe do Escornabot HFD
---

# Introdución

O taller centrarase na presentación e montaxe do Escornabot HFD, unha nova versión do clásico robot educativo con diversas vantaxes técnicas. O taller desenvolverase na zona maker da Domus o día 23 de outubro de 10 a 14 horas en versión directo e on-line simultáneos. Hai dispoñibles 20 prazas (ata un máximo de 10 en modalidade on-line).

Entregarémosvos todo o necesario para seguir o taller de montaxe do robot, aínda que quizais prefirades traer algúns utensilios ou ferramentas cos que vos sintades cómodos, como o voso propio computador, soldador, polímetro, ferramentas pequenas, etc.

Dadas as características do escornabot como ferramenta pedagóxica, darase preferencia ás persoas que traballan no mundo da educación.

## Escornabot-HFD

Escornabot HFD é un proxecto nado coa celebración [**Hardware Freedom Day**](https://en.wikipedia.org/wiki/Hardware_Freedom_Day) para mellorar, construír e conquistar o mundo co Escornabot.

Grazas ao convenio AGUSL entre a Xunta de Galicia a través da [**AMTEGA**](https://amtega.xunta.gal/) e diversas asociacións e grupos de usuarios de software libre en [**Bricolabs**](https://bricolabs.cc/) puidemos cubrir os gastos asociados ao desenvolvemento e probas do proxecto e unha pequena tirada de kits.

A celebración do HFD é a escusa perfecta para marcar fitos relacionados con FLOSHS (Free Libre Open source Hardware e Software). Con este motivo, dende [Bricolabs](https://bricolabs.cc/) propoñemos Escornabot-HFD como unha oportunidade para:

* Desenvolver unha nova versión “verde” da PCB Escornabot que poida funcionar con baterías recargables
* Recoller as experiencias do que supuxo a edición Escornabot-HFD

## Escornabot
Escornabot é un proxecto de código/hardware libre e aberto cuxo obxectivo é achegar a robótica e a programación aos nenos e nenas. Naceu en [Bricolabs](https://bricolabs.cc/) Hacklab nun memorable día de xullo de 2014. 

O Escornabot básico pode programarse cos botóns para executar secuencias de movementos. A partir de aquí, a imaxinación é o único límite nas posibilidades.

Parécese moito a algúns robots educativos comerciais, pero é moito mellor por varias razóns. Entre elas:

- Podes facelo ti cos teus alumnos, fillos, curmáns pequenos… Calquera pode axudar participando no proceso dende o principio. O pracer de fabricar o teu propio escornabot fai que sexa unha experiencia estupenda, sobre todo se hai nenos participando.
- É un proxecto de hardware aberto (OSHW) e software libre (FOSS) e podes adaptar as súas características ás túas necesidades: poñerlle sensores, leds, cambiar a apertura do ángulo de xiro, a distancia que avanza… calquera cousa que se che ocorra. Calquera (incluíndoche a ti) pode contribuír á súa evolución. Se queres achegar algún agarimo a este proxecto, en forma de novas funcionalidades, redeseño, documentación ou o que sexa, estaremos encantados de coñecer as túas ideas e suxerencias.
- É (por prezo) máis asequible que outros robots educativos comerciais. Montar un escornabot pode custar menos de 30 € se sabes onde comprar os compoñentes. Este prezo non é o seu valor porque leva tempo de traballo e outros custos indirectos como a impresión de pezas 3D.

## De onde vén o nome Escornabot? 
É unha composición acertada de escornaboi e robot (escorna + bot). Escornaboi é a palabra en galego para o *lucanus cervus* (cervo volante), o maior escaravello coñecido de europa.
