# Carga de bootloader Optiboot


**ACTUALMENTE A PLACA ESCORNABOT- HFD ENTRÉGASE CO  BOOTLOADER XA CARGADO**

Estas instrucións serán útiles se queres cambiar o bootloader, se se corrompe, ou se por calquera outro motivo é necesario volver cargalo.

Tamén é posible facer a carga do bootloader desde a aplicación ARDUINO, sen necesidade dun programador ICSP específico.


## Requisitos

- Placa Escornabot HFD
- Ordenador PC con sistema GNU/Linux
- Programador ICSP (USBasp)
- Utilidad AVRdude (descargable da Internet)
- Bootloader Optiboot (descargable da Internet)

![usbasp_programmer.jpg](img/usbasp_programmer.jpg)

## Instalación de AVRdude

Debe instalar o paquete  avrdude desde a distribución que teña na súa  PC. Por exemplo, en sistemas Debian/Ubuntu execute a seguinte liña na consola:

```
sudo apt install avrdude
```

Desde a liña de comandos do seu sistema

## Obtención do bootloader

Descarge a versión 8 do arquivo Optiboot-8.0.zip da súa páxina de Github](https://github.com/Optiboot/optiboot/releases) e selecione "Abrir co xestor de arquivos".

![optiboot_download.png](img/optiboot_download.png)

Navegue polo arquivo comprimido e selecione o subdirectorio */Optiboot-8.0/bootloaders/optiboot/*. Extraiga o ficheiro **optiboot_atmega328.hex** a una ruta onde teña acceso, por ejemplo */tmp*

![optiboot_binary.png](img/optiboot_binary.png)

## Carga de Optiboot na placa HFD

Conecte o programador USBasp no porto USB do seu PC e pique a interface de programación ICSP como se indica na foto:

![icsp_interface.jpg](img/icsp_interface.jpg)

Execute a seguinte orde desde a consola e espere uns segundos a que  AVRdude cargue o  bootloader na memoria do microcontrolador:

```
avrdude -c usbasp -p m328p -U flash:w:/tmp/optiboot_atmega328.hex -U lfuse:w:0xF7:m -U hfuse:w:0xDE:m -U efuse:w:0xFF:m -U lock:w:0x2F:m
```



