# Carga do firmware Escornabot

**ACTUALMENTE A PLACA  ESCORNABOT- HFD ENTRÉGASE CO  FIRMWARE XA CARGADO**

Terás que seguir estas instrucións se necesitas modificar ou volver cargar o  firmware no teu robot.

## Requisitos

- Placa Escornabot HFD
- Ordenador PC con sistema GNU/Linux
- Arduino IDE (descargable da Internet)
- Firmware Escornabot (descargable da Internet)
- Cable micro-USB

##  Instalar Arduino IDE

1. Vaia á [páxina oficial de Arduino](https://www.arduino.cc/), no menú _Software > Downloads_ 

   ![descargar_arduino_ide.png](img/descargar_arduino_ide.png) \

2. Seleccione a version do Arduino IDE para o seu sistema operativo (nota: nos seguintes pasos da guía describirase a instalación co sistema operativo libre GNU/Linux).

   ![download_arduino_so.png](img/download_arduino_so.png) \

3. Garde o ficheiro descargado nunha ruta do seu sistema de ficheros na que teña acceso, por ejemplo `tmp`

   ![download_linux64.png](img/download_linux64.png) \

4. Execute os seguintes comandos (axuste os nombres das rutas ao seu caso):

   ```
   cd /tmp
   tar xavf arduino-1.8.13-linux64.tar.xz
   cd arduino-1.8.13
   sudo ./install.sh
   arduino
   ``` 

Manteña o Arduino IDE aberto como aplicación de fondo, xa que se usará más adiante (ver Carga do firmware)

![arduino_ide.png](img/arduino_ide.png) \

## Obtención do firmware Escornabot

1. Vaia a páxina de descargas de  Escornabot en [páxina de descargas de Escornabot en Github](https://github.com/escornabot/arduino/releases) e  descarge a última versión dispoñible (1.6.2 nesta guía).

   ![github_escornabot.png](img/github_escornabot.png) \

2. Garde o ficheiro descargado nunha ruta do seu sistema de ficheiros na que teña acceso, por exemplo `/tmp`

3. Execute os seguintes comandos (axuste os nomes das rutas ao seu caso):

   ```
   cd /tmp
   tar xavf arduino-1.6.2.tar.gz
   mv arduino-1.6.2 firmware
   ```

## Carga del firmware en la PCB

1. Desde o IDE de Arduino (aberto no paso 1.5), vaia polo menú "Arquivo" (" File"), opción "Abrir" ("Open") e vaia ao directorio onde descargou o firmware no paso anterior (`/tmp/firmware` nesta guía).

   ![arduino_file_open.png](img/arduino_file_open.png) \

2. Abra o ficheiro `Escornabot.ino`

   ![escornabot_ino.png](img/escornabot_ino.png) \

3. Vaia polo menú "Ferramentas" ("Tools"), opción "Placa" ("Board") e seleccione "Arduino Nano"

   ![arduino_board_nano.png](img/arduino_board_nano.png) \

4. Conecte a placa de Escornabot-HFD á súa PC co cable micro-USB e vaia polo menú "Ferramentas" (" Tools"), opción "Porto" (" Port") e seleccione o porto onde ten conectado o  Escornabot (`/ dev/ ttyUSB0` nesta guía).

   ![arduino_port.png](img/arduino_port.png) \

5. Vaia polo menú "Programa" ("Sketch"), e seleccione a opción "Subir" ("Upload"). Se todo foi ben, na parte inferior da pantalla debe ver unha mensaxe que pon "Subido" ("Done uploading").

   ![arduino_done_uploading.png](img/arduino_done_uploading.png) \

6. Comprobe que o programa se executa na placa Escornabot-HFD pulsando o botón central (como primeira pulsación nada máis arrincar): debería ver e escoitar como a PCB executa o "saúdo Escornabot".

