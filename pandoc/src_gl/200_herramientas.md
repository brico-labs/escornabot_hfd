# Ferramentas

## Ferramentas para a soldadura de PCB

![Ferramentas de Soldadura](img/herramientas_soldadura.jpg){ width=75%}

__O mínimo imprescindibel__

* Soldador con seu soporte
* O teu material preferido para limpar a punta do soldador (estropallo metálico na foto)
* Estaño 
* Alicates de corte
* __Lentes de protección__
* Polímetro

__Materíal adicional__

Sempre está ben dispoñer dalgunha ferramenta para retirar o estaño se nos equivocamos, por exemplo

* Aspirador de estaño
* Malla de desoldar
* Flux (tamén pode ser útil para soldar)

Se queres que a tua placa quede impecable e limpiña tras as soldaduras, poden ser útiles:

* Alcohol isopropílico
* Cepillo de dentes
