# Montaxe do robot

## Material necesario

* A PCB HFD con todos os seus compoñentes xa soldados
* 2 motores de pasos (steppers)
* As seis pezas impresas (ver foto) .O enlace aos arquivos de impresion das pezas están dispoñibles [neste github](https://github.com/rafacouto/3d-escornabot/). Dos tres chasis Escorna CPU dispoñibles no github **só necesitas o primero: EscornaCPU 2.10-2.12 bracket escornacpu-2_10-bracket.stl**
* 16 x parafusos M3x10 (nos adoitamos usar parafusos para chave Allen (hexagonal) coñecidos como DIN912.
* 2 porcas M3
* A chave para apretar os parafusos

## Material adicional

Este material non é imprescindible pero facilita moito a montaxe.

* Soldador ou algunh xeito de quentar as porcas para insertalas nas pezas impresas.
* Un escariador ou unha broca de 3mm para repasar a boca dos furados dos parafusos nas pezas impresas en 3D (tamen podes amañar cun desparafusador pequeno ou similar)

![Pezas](img/piezas.jpg){ width=50% }

## Montaxe

Para proceder coa montaxe de todas as pezas de impresión 3D e da PCB convén revisar previamente todos os orificios para os parafusos de todas as pezas con algunha ferramenta que nos permita ensanchar lixeiramente a boca dos furados. Un desparafusador ou un *cutter* pequeno pódenos valer perfectamente.

![Quitando rebabas](img/escariador.jpg){ width=50% }

Tamén é ben insertar os parafusos nos seus furados coas pezas separadas e darlles un par de vueltas coa chave (¡non mais!) para que deixen a rosca marcada na boca do furado, eso facilitará a insercios dos parafusos mais adiante.

### Pasos da montaxe

O primeiro paso é montar os motores no seu soporte.

![Soporte de motores](img/porta_motores_a.jpg){ width=50% } \

Comproba que os cableados dos motores saian pola fronte do  portamotores. É dicir, ao lado en que en orificio do parafuso é máis grande para permitir que o parafuso quede  encastrado.

Na seguinte foto poderás ver os motores xa montados.

Despois de montar os motores no portamotores, debemos unir o portamotores có  arco do chasis (é a peza que soportará o portapilas)

![Motores e Arco do Chasis](img/porta_motores_b.jpg){ width=50% } \

Có portamotores xa unido ao arco do chasis, damoslle volta a todo o conxunto para poñer os parafusos da peza da bola-tola.

![Chasis (vista inferior)](img/chasis_a.jpg){ width=50% } \

O soporte da bola-tola pechará o arco do chasis, o encastre da bola quedará na parte traseira do robot.

![Soporte de la bola](img/chasis_b.jpg){ width=50% } \

Damoslle de novo a volta ao noso Escornabot para poñerlle o porta-PCB

![Chasis (vista superior)](img/chasis_c.jpg){ width=50% } \

O porta-PCB debe orientarse cos brazos asimétricos cara ao frontal do robot

![Soporte da PCB](img/chasis_d.jpg){ width=50% } \

E procedemos a atornillar a PCB ao chasis do robot. 

![Soporte da PCB (vista superior)](img/porta_pcb.jpg){ .alignleft width=50% } ![PCB no seu soporte](img/porta_pcb_b.jpg){ width=50% }

Agora debemos insertar as porcas M3 nas suas posicións dentro do cubo das rodas. Para facelo o mellor e usar un soldador, alicates finos e algunha ferramenta que nos permita quentar as porcas  e exercer un pouco de presión nelas para insertarlas no seu sitio, alineadas có orificio do parafuso.

![Porca da  roda](img/rueda_tuerca.jpg){ width=50% } \

Unha vez colocadas as porcas non temos  mais  que fixalas aos eixos dos motores cun parafuso cada unha. Hai que comprobar que o parafuso fai tope co rebaixe plano do eixo do motor.

Conectamos motores (asegúrache de que os  conectores entran na posición, e orientación correcta, facendo coincidir as marcas do  conector co dos  conectores instalados na placa )

![Escornabot Rematado](img/escornabot.jpg){ width=50% } \
