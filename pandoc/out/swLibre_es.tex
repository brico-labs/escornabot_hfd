% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}
%
\documentclass[
  12pt,
  spanish,
]{scrartcl}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
  \setmainfont[]{Ubuntu}
  \setmonofont[]{Ubuntu Mono}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Taller Escornabot 2021},
  pdfauthor={Bricolabs},
  pdflang={es-ES},
  colorlinks=true,
  linkcolor=Maroon,
  filecolor=Maroon,
  citecolor=Blue,
  urlcolor=Blue,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage[a4paper]{geometry}
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{5}
\ifxetex
  % Load polyglossia as late as possible: uses bidi with RTL langages (e.g. Hebrew, Arabic)
  \usepackage{polyglossia}
  \setmainlanguage[]{spanish}
\else
  \usepackage[shorthands=off,main=spanish]{babel}
\fi

\title{Taller Escornabot 2021}
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
\subtitle{Software y Hardware Libres}
\author{Bricolabs}
\date{octubre-2021}

\begin{document}
\maketitle
\begin{abstract}
Algunas aclaraciones al respecto del software y hardware libres
\end{abstract}

{
\hypersetup{linkcolor=}
\setcounter{tocdepth}{3}
\tableofcontents
}
La asociación Bricolabs tiene como uno de sus objetivos principales la
difusión del software y hardware libres. En este documento intentamos
dar una introducción a esos conceptos.

\hypertarget{software-libre}{%
\section{Software Libre}\label{software-libre}}

El \emph{Software Libre} es algo muy concreto: un software es
\emph{Software Libre} si y solo si garantiza \textbf{al usuario final}
las \textbf{\emph{Cuatro Libertades}}:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{-1}
\tightlist
\item
  Ejecutar el programa como se desee y con cualquier propósito
\item
  Estudiar cómo funciona el programa y modificarlo para que haga lo que
  se desee
\item
  Redistribuir copias para ayudar a otros
\item
  Redistribuir copias modificadas para terceros
\end{enumerate}

A veces, de forma resumida, se habla de las libertades de \textbf{Uso,
Modificación y Copia}.

La definición del \emph{Software Libre} se centra en los derechos del
usuario que no pueden ser limitados por el programador o el
distribuidor, en cambio en el software privativo normalmente se
establecen límites a lo que el usuario puede hacer con ese software.

Las libertades 1 y 3 implican que hay que dar al usuario acceso completo
al código fuente.

Podemos hablar de diversas licencias de software y de cuanto se acercan
o alejan a la definición del \emph{Software Libre}, pero si no cumplen
la condición de garantizar \textbf{\emph{Las Cuatro Libertades}} no son
\emph{Software Libre}

Específicamente, el \emph{Open Source Software} (OSS o Software de
Código Abierto en español), no es \emph{Software Libre}.

\hypertarget{uso-comercial}{%
\subsection{Uso comercial}\label{uso-comercial}}

En inglés \emph{Free Software} puede significar \emph{Software Gratis} o
\emph{Software Libre}, esto ha provocado todo tipo de confusiones y
malentendidos. De hecho ahora se tiende a usar la expresión
\textbf{\emph{Free Libre Software}} en lugar de \emph{Free Software}
para que no queden dudas de que se trata de \emph{Free as in Freedom}.
En castellano no tenemos ese problema, pero recordemos que
\emph{Software Libre} no tiene por que ser software gratuito, y que
tampoco todo el software gratis es \emph{Software Libre}.

En realidad \textbf{\emph{Las Cuatro Libertades}} garantizan el Uso
Comercial. Las libertades 2 y 3 permiten la redistribución de cualquier
Software Libre, y nadie impide cobrar por ello.

Ahora bien, para que el software que redistribuyes sea \emph{Software
Libre} no puedes limitar de ninguna manera \textbf{\emph{Las Cuatro
Libertades}}.

\hypertarget{software-complejo}{%
\subsection{Software Complejo}\label{software-complejo}}

Lo normal es que el software se componga de muchas partes: bibliotecas
(o librerias), sistemas operativos, programas de utilidad etc. etc.

El conjunto del software será libre si y solo si todos sus componentes
son \emph{Software Libre}

\hypertarget{copyleft}{%
\subsection{Copyleft}\label{copyleft}}

El Copyleft es un método para exigir que las versiones modificadas y/o
extendidas de un \emph{Software Libre} sean a su vez \emph{Software
Libre}; es decir se exige que cualquiera que redistribuya el software,
con o sin cambios, garantice a los receptores \textbf{\emph{Las Cuatro
Libertades}}.

Un software, o más bien una licencia, que especifica que las copias
modificadas no son libres (limitando de cualquier forma
\textbf{\emph{Las Cuatro Libertades}}) no es una Licencia Libre, no es
\emph{Software Libre}

Si la licencia exige que las versiones redistribuidas sean a su vez
libres, entonces si se puede considerar \emph{Software Libre}.

\hypertarget{hardware-libre}{%
\section{Hardware Libre}\label{hardware-libre}}

En principio parece deseable (al menos para mucha gente, incluidos
nosotros) extender la definición del \emph{Software Libre} al Hardware.
El problema es que el software es una ``propiedad intelectual'' casi
pura. Tanto el diseño (código fuente) como el producto final
(ejecutable) de un software son fáciles de copiar y distribuir,
prácticamente sin coste.

El hardware no es así en absoluto, siempre hay costes asociados que
además pueden ser muy grandes. Esta característica por sí sola ya
impediría usar la definición de \emph{Software Libre} para el
\emph{Hardware Libre}.

Lo que si parece transladable es la filosofía del \emph{Software Libre}.
Por ejemplo, no sería difícil aplicar la filosofía y definición del
\emph{Software Libre} a los diseños del Hardware.

Dada la naturaleza del hardware y sus problemas asociados veremos que
hay varias líneas de trabajo en la definición del \emph{Hardware Libre}.
Además todavía no se avanza en todos los campos por igual, aunque hay
proyectos en distintas áreas de la tecnología, la mayor parte de los
intentos de definición de hardware libre y de implementación de
proyectos libres se centra o está relacionado con la electrónica.

\hypertarget{dispositivos-de-luxf3gica-programable}{%
\subsection{Dispositivos de lógica
programable}\label{dispositivos-de-luxf3gica-programable}}

En el hardware además de los problemas de costes asociados que hemos
mencionado, normalmente estamos hablando de sistemas complejos (análogos
al software complejo que también mencionamos) Pensemos en todos los
componentes que necesitamos para una impresora 3D o un automóvil por
citar un proyecto más ambicioso.

Tomemos un ejemplo de electrónica y además muy conocido: la placa
Arduino. Es un ejemplo típico de Hardware Libre, pero el microprocesador
ATMega que se usa para implementar el Arduino no es, de ninguna manera,
libre. De la misma forma una impresora 3D usa motores, servos, etc que a
su vez no son hardware libre. Si pensamos en un automovil tendríamos
patentes incluso para distintos tipos de acero.

En lo que respecta a la electrónica, en los últimos años hemos visto
grandes avances en la definición de dispositivos de lógica programable
\textbf{libres}, ahí tenemos por ejemplo la FPGA Alhambra (ahora ya hay
más) o el software de programación Icestudio. Con una FPGA sería posible
diseñar una placa con las mismas funcionalidades que un Arduino, o que
una tarjeta gráfica de ordenador, o incluso el procesador del propio
ordenador, por citar algunas posibilidades.

Estos avances hacen posible el desarrollo de dispositivos electrónicos
que serían completamente libres, en el sentido de no tener que usar
necesariamente componentes cerrados.

\hypertarget{open-hardware}{%
\subsection{Open Hardware}\label{open-hardware}}

Igual que surge el Open Source, como un intento de adoptar el ciclo de
desarrollo del \emph{Software Libre} para el software privativo. También
se habla de \emph{Open Hardware} y cada vez hay más interés (y presión)
por parte de la industria para definir casos de \emph{Open Hardware}.
Hoy en día ya se despliegan \emph{Software Defined Networks} (SDN) por
parte de los operadores de telecomunicaciones y proveedores de servicios
en la nube. Ahí interesa tener el hardware: servidores, routers,
estaciones base de telefonía, etc. etc. definidos como Open Hardware, de
manera que la red no dependa de un solo fabricante, se bajen costes,
haya repuestos compatibles, etc. etc.

Es debatible hasta qué punto estos movimientos de la industria encajan
con la filosofía del \emph{Software Libre} de anteponer a todo los
derechos del usuario final. Pero en cualquier caso la tendencia existe,
y también puede suponer una oportunidad para el \emph{Hardware Libre}.

\hypertarget{proyecto-escornabot}{%
\section{Proyecto Escornabot}\label{proyecto-escornabot}}

En el proyecto Escornabot intentamos ceñirnos lo más posible a la
filosofía del \emph{Software Libre}. En el apartado del software no hay
problemas, usamos la licencia
\href{https://github.com/escornabot/arduino/commit/0315f2333f7aecb6eb785dcef96e3bd342fbc536}{GPL
V2}. En el apartado del hardware intentamos que sea un
\textbf{\emph{Hardware de Diseño Abierto}}, publicando todo el diseño de
la electrónica y las piezas impresas y usando para todo la licencia
\href{https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2}{Cern
Open Hardware License version 2}.

\end{document}
