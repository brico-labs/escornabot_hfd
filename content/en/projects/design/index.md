---
title: "Design"
weight: 1
resources:
    - src: escornaBlack.jpg
      params:
          weight: -100
---

The Escornabot_HFD PCB design is focused on:

* making the board compatible with the use of rechargeable batteries
* having a board that, without compromising the integration level, serves
  as the basis for a PCB assembly (soldering) workshop,
  thus expanding the educational possibilities of the PCB
* use components easy to find on the market
