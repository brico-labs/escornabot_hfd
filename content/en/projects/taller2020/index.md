---
title: "Workshop: Escornabot HFD PCB soldering"
weight: 2
---

## PCB assembly workshop 'HFD Escornabot'

### The new Escornabot HFD plate

The new plate for Escornabot (code name 'Escornabot HFD') has been manufactured thanks to the sponsorship of [AMTEGA](https://amtega.xunta.gal/), in the frame of the AGUSL-2020 agreement.

### Objective of the workshop

In this workshop we want to check what difficulties any person who signs up for future workshops for soldering Escornabot HFD PCBs will have to face.

### Additional information

All instructions for mounting the plate can be found at [the workshop entry](https://wiki.escornabot.com/es/hfd-2020) on the HFD Scoreboard wiki


Translated with www.DeepL.com/Translator (free version)
