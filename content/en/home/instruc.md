---
title: "Build-up instructions"
weight: 30
headless: false
---

Detailed instructions to build up a Escornabot HFD robot are available in [our wiki](https://wiki.escornabot.com/es/hfd-2020) (only spanish version, sorry)
