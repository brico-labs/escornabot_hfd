---
title: "About"
image: "logo.png"
weight: 8
---

Escornabot HFD is a project born with the celebration [**H**ardware
**F**reedom
**D**ay](https://en.wikipedia.org/wiki/Hardware_Freedom_Day) to
improve, build and conquer the world with the Escornabot.

Thanks to the AGUSL 2020 agreement between
[AMTEGA](https://amtega.xunta.gal/) and various associations and
groups of users of free software, we have been able to cover the costs
associated with the development and testing of the project and a small
print run of kits to distribute among interested schools.

The celebration of the HFD is the perfect excuse to set milestones
related to FLOSHS (Free Libre Open Source Hardware and Software). With
this in mind, [Bricolabs](https://bricolabs.cc) is considering
Escornabot HFD 2020 as an opportunity to:

* Develop a new "green" version of the Escornabot PCB that can run on
  rechargeable batteries
* Launch a challenge to print in 3D dozens of Escornabot bodies with
  the help of the Maker Community
* Delivering Escornabot kits together with printed pieces to
  educational and special interest centres throughout the country
* Collect the experiences of the Escornabot HFD 2020 edition
