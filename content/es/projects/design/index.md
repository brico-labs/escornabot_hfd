---
title: "Diseño"
weight: 1
resources:
    - src: escornaBlack.jpg
      params:
          weight: -100
---

El diseño de la PCB Escornabot HFD está centrado en:

* hacer la placa compatible con el uso de baterias recargables.
* tener una placa que sin comprometer el nivel de integración, sirva
  como base para un taller de montaje (soldadura) de las PCB,
  ampliando así las posibilidades didácticas de la misma
* optimizar los componentes utilizados en la placa con objetivo de que
  sean fáciles de encontrar en el mercado
