---
title: "Taller de montaje de PCB"
weight: 2
---

## Taller de montaje de la PCB ""Escornabot HFD""

### La nueva placa Escornabot HFD

La nueva placa para Escornabot con nombre clave "Escornabot HFD" ha sido fabricada gracias al patrocinio de [AMTEGA](https://amtega.xunta.gal/), gracias al convenio AGUSL-2020.

### Objetivo del taller

En este taller queremos comprobar que dificultades tendrán que enfrentar cualquier persona que se apunte a los futuros talleres para soldadura de las PCB Escornabot HFD.

### Info adicional

Todas las intrucciones de montaje de la placa pueden consultarse en [la entrada correspondiente al taller](https://wiki.escornabot.com/es/hfd-2020) en la wiki del Escornabot HFD
