---
title: "Taller Escornabot HFD"
image: "logo.png"
weight: 1
---

## **PLAZAS AGOTADAS - CERRADA LA INSCRIPCIÓN AL TALLER**
El próximo **sábado 23 de octubre de 10 a 14 horas en la zona Maker del Museo Domus de A Coruña**, tenemos taller de Escornabot HFD, versión presencial y online (simultáneamente). Ofertamos 20 plazas con un máximo de 10 plazas en modalidad _on line_.

El taller se centrará en la presentación y montaje del Escornabot HFD, una nueva versión del clásico robot educativo con algunas ventajas técnicas.

Os entragaremos todo el material necesario para seguir el taller de montaje del robot, aunque si preferís usar vuestras propias herramientas no hay inconveniente (ordenador propio, soldador, polímetro, herramientas pequeñas, etc.)

Dadas las caracteristicas como recurso pedagógico del Escornabot **daremos preferencia a las personas que trabajan en el ámbito de la educación**.

