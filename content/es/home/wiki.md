---
title: "Wiki"
weight: 20
headless: false
---

Puedes consultar la documentación detallada del proyecto en [nuestra wiki](https://wiki.escornabot.com/es/home)
