---
title: "Instrucciones de Montaje"
weight: 30
headless: false
---

Las instrucciones detalladas para montar un robot Escornabot HFD están disponibles en [nuestra wiki](https://wiki.escornabot.com/es/hfd-2020)
