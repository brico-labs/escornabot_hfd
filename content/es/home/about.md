---
title: "Sobre"
image: "logo.png"
weight: 8
---

Escornabot HFD es un proyecto nacido con la celebración [**H**ardware
**F**reedom **D**ay]([**H**ardware
**F**reedom **D**ay](https://en.wikipedia.org/wiki/Hardware_Freedom_Day) para
mejorar, construir y conquistar el mundo con el Escornabot.

Gracias al convenio AGUSL 2020 entre
[AMTEGA](https://amtega.xunta.gal/) y diversas asociaciones y grupos
de usuarios de software libre hemos podido cubrir los gastos asociados
al desarrollo y pruebas del proyecto y una pequeña tirada de kits para
repartir entre los colegios interesados.

La celebración del HFD es la excusa perfecta para marcar hitos
relacionados con _FLOSHS (Free Libre Open Source Hardware y
Software)_. Con este motivo, desde [Bricolabs](https://bricolabs.cc)
nos planteamos Escornabot HFD 2020 como una oportunidad para:

* Desarrollar una nueva versión "verde" de la PCB Escornabot que pueda
  funcionar con baterías recargables
* Lanzar un reto para imprimir en 3D decenas de cuerpos de Escornabot
  con la ayuda de la Comunidad Maker
* Repartir los kits Escornabot junto con las piezas impresas a centros
  educativos y de interés especial de todo el país
* Recoger las experiencias de lo que ha supuesto la edición Escornabot
  HFD 2020
