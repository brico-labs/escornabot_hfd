---
title: "Sobre"
image: "logo.png"
weight: 8
---

Escornabot HFD é un proxecto ideado para a celabración do [**H**ardware **F**reedom **D**ay]([**H**ardware **F**reedom **D**ay](https://en.wikipedia.org/wiki/Hardware_Freedom_Day) para mellorar, construir e conquistar o mundo có Escornabot.

Grazas ao convenio AGUSL 2020 promovido e patrocinado pola [AMTEGA](https://amtega.xunta.gal/) e dirixido a  diversas asociacións e grupos de usuarios de software libre fumos quen de  cubrir os gastos asociados ao desenvolvemento, as probas do proxecto, e a fabricación dunha pequena tirada de kits para repartir entre os colexios interesados.

A celebración do HFD é a excusa perfecta para marcar os fitos relacionados có _FLOSHS (Free Libre Open Source Hardware e Software)_. Con este motivo, dende [Bricolabs](https://bricolabs.cc) plantexámonos o Escornabot HFD 2020 como unha oportunidade para:

* Desenvolver unha nova versión "verde" da PCB Escornabot que poida funcionar con baterías recargables
* Lanzar un reto para imprimir en 3D decenas de corpos de Escornabot coa axuda da Comunidade Maker
* Repartir os kits Escornabot xunto coas pezas impresas a centros  educativos y de interese especial de todo o país
* Recoller as experiencias do que supuxo a edición Escornabot HFD 2020
