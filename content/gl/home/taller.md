---
title: "Obradoiro Escornabot HFD"
image: "logo.png"
weight: 1
---

## **PRAZAS AGOTADAS -- PECHADA A INSCRIPCIÓN AO OBRADOIRO**

O vindeiro **sábado 23 de outubro de 10 a 14 horas na zona Maker do Museo Domus de A Coruña**, temos taller de Escornabot, en versión presencial e on-line simultáneamente. Hai dispoñibles 20 prazas (ata un máximo de 10 en modalidade on-line).

O taller centrarase na presentación e montaxe do Escornabot HFD, unha nova versión do clásico robot educativo con diversas vantaxes técnicas. O taller desenvolverase na zona maker da Domus o día 

Entregarémosvos todo o necesario para seguir o taller de montaxe do robot, aínda que quizais prefirades traer algúns utensilios ou ferramentas cos que vos sintades cómodos, como o voso propio computador, soldador, polímetro, ferramentas pequenas, etc.

Dadas as características de ferramenta pedagóxica do escornabot, **darase preferencia ás persoas que traballan no mundo da educación**.

