---
title: "Instruccións de Montaxe"
weight: 30
headless: false
---

As instruccións detalladas para montar un robot Escornabot HFD están dispoñibles na [nosa wiki](https://wiki.escornabot.com/es/hfd-2020)
