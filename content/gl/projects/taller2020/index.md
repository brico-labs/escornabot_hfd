---
title: "Obradoiro de montaxe PCBs"
weight: 2
---

## Taller de montaxe da PCB ""Escornabot HFD""

### A nova placa Escornabot HFD

A nova placa para Escornabot con nome crave "Escornabot HFD" foi fabricada grazas ao patrocinio de [AMTEGA](https://amtega.xunta.gal/), grazas ao convenio AGUSL-2020.

### Obxectivo do taller

Neste taller queremos comprobar que dificultades terán que enfrontar calquera persoa que se apunte aos futuros talleres para soldadura das PCB Escornabot HFD.

### Info adicional

Todas as intrucciones de montaxe da placa poden consultarse en [a entrada correspondente ao taller](https://wiki.escornabot.com/é/hfd-2020) na wiki do Escornabot HFD

