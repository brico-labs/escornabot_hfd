---
title: "Deseño"
weight: 1
resources:
    - src: escornaBlack.jpg
      params:
          weight: -100
---


O deseño da PCB Escornabot HFD está centrado en:

* facer a placa compatible co uso de baterias recargables.
* ter unha placa que sen comprometer o nivel de integración, sirva
  como base para un obradoiro de montaxe (soldadura) das PCB,
  ampliando así as posibilidades didácticas da mesma
* optimizar os compoñentes utilizados na placa con obxectivo de que
  sexan fáciles de atopar no mercado
